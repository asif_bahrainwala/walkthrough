***This code is for learning purpose only***
tested on Linux Mint 21, Intel Pentium Silver J5040 (RAM: 4GB)
requires QMake to generate make files (I recommend using QTCreator)

Install
run in terminal: sudo apt-get install libglfw3-dev make g++
Install QT (latest from https://www.qt.io/download-open-source), during setup, please ensure that you have a QT build system (you may select "Desktop gcc 64-bit" during package selection).


features
1) WASD + mouse control
2) Bloom effect
3) 3 Point lights (not deferred)
4) code is structured to add new frame buffers (for post processing) easily.
5) GL objects are cleaned up using custom smart objects.
6) removed calllist  (as this is a legacy thing), added VAO
7) Added SkyBox
8) C/V to toggle camera dance
9) added support form imGUI : https://github.com/ocornut/imgui.git
Image from https://pixabay.com/
