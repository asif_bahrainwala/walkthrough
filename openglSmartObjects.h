#ifndef OPENGLSMARTOBJECTS_H
#define OPENGLSMARTOBJECTS_H

#include <string>
#include <string.h>
#include <iostream>
#include <vector>
#include <memory>
#include <sstream>
#include <fstream>
#define GL_GLEXT_PROTOTYPES

#include <GL/glx.h>
#include <GL/gl.h>
#include <GL/glext.h>

#include "soil/src/SOIL.h"

#include <glm/vec3.hpp> // glm::vec3
#include <glm/vec4.hpp> // glm::vec4
#include <glm/mat4x4.hpp> // glm::mat4
#include <glm/gtc/matrix_transform.hpp>

#define WIDTH 1500
#define HEIGHT 900

extern glm::vec3 cameraPos;
extern glm::mat4 g_View;
extern glm::mat4 g_Projection;
extern glm::mat4 g_viewProjec;
extern float g_step;
extern bool cameraDance;

using namespace std;

extern int g_count; //we use this to check for object leaks

class Texture
{  
    GLuint m_textureId=0;
    bool m_isCubicTexture=false;
public:
    Texture(GLuint textureId):m_textureId(textureId){++g_count;}
    Texture(int w,int h)
    {
        ++g_count;
        glGenTextures(1,&m_textureId);
        glBindTexture(GL_TEXTURE_2D, m_textureId);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
        glTexStorage2D(GL_TEXTURE_2D, 10, GL_RGBA8, w, h);
    }
    Texture(const vector<string> &cubetextures)
    {
        if(cubetextures.size()!=6) throw "require 6 faces to create cube texture";

        m_isCubicTexture=true;
        ++g_count;
        m_textureId = SOIL_load_OGL_cubemap(cubetextures[0].c_str(),
                                                   cubetextures[1].c_str(),
                                                   cubetextures[2].c_str(),
                                                   cubetextures[3].c_str(),
                                                   cubetextures[4].c_str(),
                                                   cubetextures[5].c_str(),SOIL_LOAD_RGB,SOIL_CREATE_NEW_ID,SOIL_FLAG_MIPMAPS);
    }
    Texture(const string &s)
    {
        ++g_count;

        //refer: https://www.khronos.org/opengl/wiki/Common_Mistakes#Creating_a_complete_texture
        m_textureId=SOIL_load_OGL_texture(s.c_str(),SOIL_LOAD_AUTO,SOIL_CREATE_NEW_ID,SOIL_FLAG_MIPMAPS | SOIL_FLAG_INVERT_Y | SOIL_FLAG_NTSC_SAFE_RGB | SOIL_FLAG_COMPRESS_TO_DXT);
        glGenerateMipmap(GL_TEXTURE_2D);
    }

    GLuint GetID() {return m_textureId;}

    void UseTexture(GLenum stage=GL_TEXTURE0)
    {
        glActiveTexture(stage);
        if(m_isCubicTexture)
            glBindTexture(GL_TEXTURE_CUBE_MAP, m_textureId);
        else
            glBindTexture(GL_TEXTURE_2D, m_textureId);
    }

    string SaveToFile()
    {
        if(m_isCubicTexture) throw "Not supported for cube texture";

        int w=0,h=0;
        glBindTexture(GL_TEXTURE_2D, m_textureId);
        glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_WIDTH,  &w);glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_HEIGHT, &h);
        unsigned char data[w*h*3]={};
        glPixelStorei(GL_UNPACK_ALIGNMENT, 1); //https://www.khronos.org/opengl/wiki/Common_Mistakes
        glGetTexImage(GL_TEXTURE_2D,0, GL_RGB,GL_UNSIGNED_BYTE,(GLvoid*)data);
        stringstream ss;
        ss<<this<<"_image.bmp";
        SOIL_save_image(ss.str().c_str(),SOIL_SAVE_TYPE_BMP,w,h,3,data);

        return ss.str();
    }

    ~Texture()
    {
        --g_count;
        glDeleteTextures(1,&m_textureId);
    }
};

class Program
{
    GLuint m_programID = glCreateProgram();

    static GLuint shaderCompileFromFile(GLenum type,const char *source);
    void shaderAttachFromFile(GLenum type, const char *shaderSource);
    void GetLinkStatus();
public:
    Program(){++g_count;}
    void setShaderSource(const char *vertexShaderSrc,const char *HullShaderSrc,const char *DomainShaderSrc,const char *geomShaderSrc,const char *fragmentShaderSrc)
    {
        shaderAttachFromFile(GL_VERTEX_SHADER,vertexShaderSrc);
        shaderAttachFromFile(GL_TESS_CONTROL_SHADER,HullShaderSrc);
        shaderAttachFromFile(GL_TESS_EVALUATION_SHADER,DomainShaderSrc);
        shaderAttachFromFile(GL_GEOMETRY_SHADER,geomShaderSrc);
        shaderAttachFromFile(GL_FRAGMENT_SHADER,fragmentShaderSrc);
    }

    void linkProgram()
    {
        glLinkProgram(m_programID);
        GetLinkStatus();
    }

    ~Program()
    {
        --g_count;
        glDeleteProgram(m_programID);
    }

    GLuint GetID(){return m_programID;}
};

//creating the frameBuffer
class frameBuffer
{
    GLuint m_FrameBuffer=0;
    vector<GLenum> m_DrawBuffers;
    size_t m_width=0,m_height=0;
public:
    vector<shared_ptr<Texture>> m_tex; //always work with reference counted pointers
    shared_ptr<Texture> m_depthtex;

    ~frameBuffer()
    {
        --g_count;
        glDeleteFramebuffers(1,&m_FrameBuffer);
    }

    frameBuffer(size_t colorbuffers=1)
    {
        ++g_count;
        for(size_t i=0;i<colorbuffers;++i)
        {
            shared_ptr<Texture> p(new Texture(WIDTH,HEIGHT));
            m_tex.push_back(p);
        }
        GLuint depth_tex=0;
        glGenTextures(1, &depth_tex);
        glBindTexture(GL_TEXTURE_2D, depth_tex);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT24, WIDTH,HEIGHT, 0, GL_DEPTH_COMPONENT, GL_UNSIGNED_BYTE, NULL); //NULL means reserve texture memory, but texels are undefined

        glGenFramebuffers(1, &m_FrameBuffer);
        glBindFramebuffer(GL_FRAMEBUFFER, m_FrameBuffer);

        //attach the color buffers
        for(size_t i=0;i<colorbuffers;++i)
            glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0+i, GL_TEXTURE_2D, m_tex[i]->GetID(), 0/*mipmap level*/);

        //Attach depth texture to FBO
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depth_tex, 0/*mipmap level*/);

        for(size_t i=0;i<m_tex.size();++i)
            m_DrawBuffers.push_back(GL_COLOR_ATTACHMENT0+i);

        m_depthtex=make_shared<Texture>(depth_tex);
        m_width=WIDTH;
        m_height=HEIGHT;
    }

    int m_viewport[4];
    void UseFrameBuffer()
    {
        glGetIntegerv(GL_VIEWPORT,m_viewport);

        glViewport(0,0,m_width,m_height);
        glBindFramebuffer(GL_FRAMEBUFFER, m_FrameBuffer);  //enable frame buffer
        glDrawBuffers(m_DrawBuffers.size(), &m_DrawBuffers[0]);
    }

    void StopUseFrameBuffer()
    {
        glViewport(m_viewport[0],m_viewport[1],m_viewport[2],m_viewport[3]);  //warning FBs are not resized

        glBindFramebuffer(GL_FRAMEBUFFER, 0);  //enable frame buffer

        for(auto i:m_tex)
        {
            i->UseTexture();
            glGenerateMipmap(GL_TEXTURE_2D);  //update mipmaps
        }
    }
};

struct SpotLight
{
    glm::vec3 m_spotlightcol    =glm::vec3(0,0,0);
    glm::vec3 m_spotlightpos    =glm::vec3(0,0,0);
    glm::vec3 m_spotlightlookat =glm::vec3(0,0,0);
};

struct LightPoint
{
    ~LightPoint(){--g_count;}
    LightPoint(glm::vec4 pos,glm::vec3 lightcol):m_pos(pos),m_orgpos(pos),m_lightcolor(lightcol){++g_count;}
    glm::vec4 m_pos;
    glm::vec4 m_orgpos;
    glm::vec3 m_lightcolor;
};

struct MyVertex
{
    MyVertex(){}
    MyVertex(float i,float j,float k,float ni,float nj,float nk,float l,float m):
        x(i),y(j),z(k),
        nx(ni),ny(nj),nz(nk),
        tx(l),ty(m)
    {}
    float x, y, z;        //Vertex
    float nx, ny, nz;     //Normal
    float tx,ty;          //for texture stage 1
};

struct Geom
{
    GLuint m_Index=0,m_vertex=0;
    GLuint m_arrayvertexobject=0;

    bool m_MovewithEye=false,m_arrayvertexobjectCreated=false;
    size_t m_numberOfIndicestobeDrawn=0,m_numberOfVerticestobeDrawn=0;

    shared_ptr<SpotLight> m_spotLight;
    shared_ptr<Program> m_program=0;
    vector<shared_ptr<Texture>> m_tex;
    vector<shared_ptr<LightPoint>> m_lightpoints;

    //GLuint m_calllist=glGenLists(1);  //call list are part of legacy , so we will remove this
    Geom(shared_ptr<Program> p):m_program(p){
            auto progID=m_program->GetID();
            glBindAttribLocation(progID,0,"pos");
            glBindAttribLocation(progID,2,"normal");
            glBindAttribLocation(progID,1,"tex");

            m_program->linkProgram();
            glGenVertexArrays(1,&m_arrayvertexobject);

            ++g_count;
    }

    virtual ~Geom()
    {
        --g_count;
        glDeleteBuffers(1,&m_vertex);m_vertex=0;
        glDeleteBuffers(1,&m_Index);m_Index=0;
        glDeleteVertexArrays(1,&m_arrayvertexobject);m_arrayvertexobject=0;
    }
    virtual void Draw(bool bTransparent=false,glm::mat4=glm::identity<glm::mat4>());
};


#endif // OPENGLSMARTOBJECTS_H
