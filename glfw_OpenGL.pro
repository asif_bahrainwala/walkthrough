TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

#for address santization
CONFIG += sanitizer sanitize_address
LIBS += -static-libasan

SOURCES += \
        GLFWWindow.cpp \
        main.cpp \
        openglSmartObjects.cpp

SOURCES += soil/src/SOIL.c
SOURCES += soil/src/stb_image_aug.c
SOURCES += soil/src/image_helper.c
SOURCES += soil/src/image_DXT.c
SOURCES += ../imgui/*.cpp
SOURCES += ../imgui/backends/imgui_impl_opengl3.cpp
SOURCES += ../imgui/backends/imgui_impl_glfw.cpp

LIBS+= -lglfw -lrt -lm -ldl -lGL -lpthread

HEADERS += \
    GLFWWindow.h \
    Geometry/Frame/Frame.h \
    Geometry/Light/Light.h \
    Geometry/common/randome_helper.h \
    Geometry/common/shaders.h \
    Geometry/cube/cube.h \
    Geometry/floor/floor.h \
    Geometry/walls/wall.h \
    openglSmartObjects.h

INCLUDEPATH += glm
INCLUDEPATH += ../imgui
INCLUDEPATH += ../imgui/backends

QMAKE_CXXFLAGS += -fopenmp
LIBS += -fopenmp
