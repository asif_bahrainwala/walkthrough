#include "GLFWWindow.h"

//many thanks to https://learnopengl.com/Getting-started/Camera

glm::mat4 g_View= glm::identity<glm::mat4>();
glm::mat4 g_Projection = glm::perspective(glm::radians(45.0f),((float) WIDTH )/ HEIGHT, 0.1f, 100.f);
glm::mat4 g_viewProjec=glm::identity<glm::mat4>();

glm::vec3 cameraPos   = glm::vec3(0.0f, 0.0f,  3.0f);
static glm::vec3 cameraFront = glm::vec3(0.0f, 0.0f, -1.0f);
static glm::vec3 cameraUp    = glm::vec3(0.0f, 1.0f,  0.0f);

bool cameraDance=false;

void resetGLViewport(GLFWwindow *window)
{
    int width=0;
    int height=0;
    glfwGetFramebufferSize(window, &width, &height);
    glViewport(0,0,width,height);  //warning FBs are not resized
}

void OpenGLWindow::key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    const float cameraSpeed = 0.05f; // adjust accordingly
    if ('W'==key)
        cameraPos += cameraSpeed * cameraFront;
    if ('S'==key)
        cameraPos -= cameraSpeed * cameraFront;
    if ('A'==key)
        cameraPos -= glm::normalize(glm::cross(cameraFront, cameraUp)) * cameraSpeed;
    if ('D'==key)
        cameraPos += glm::normalize(glm::cross(cameraFront, cameraUp)) * cameraSpeed;
    if ('C'==key)
        cameraDance=true;
    if ('V'==key)
        cameraDance=false;

    g_View= glm::lookAt(cameraPos,cameraPos + cameraFront,glm::vec3(0,1,0));
    g_viewProjec=g_Projection*g_View;

    resetGLViewport(window);
}

void OpenGLWindow::mouse_callback(GLFWwindow* window, double xpos, double ypos)
{
    static float lastX = WIDTH/2, lastY = HEIGHT/2;
    static bool firstMouse =true;
    if (firstMouse)
    {
        lastX = xpos;
        lastY = ypos;
        firstMouse = false;
    }

    float xoffset = xpos - lastX;
    float yoffset = lastY - ypos;
    lastX = xpos;
    lastY = ypos;

    float sensitivity = 0.4f;
    xoffset *= sensitivity;
    yoffset *= sensitivity;

    static float yaw=-90,pitch=0;
    yaw   += xoffset;
    pitch += yoffset;

    if(pitch > 89.0f)
        pitch = 89.0f;
    if(pitch < -89.0f)
        pitch = -89.0f;

    glm::vec3 direction;
    direction.x = cos(glm::radians(yaw)) * cos(glm::radians(pitch));
    direction.y = sin(glm::radians(pitch));
    direction.z = sin(glm::radians(yaw)) * cos(glm::radians(pitch));
    cameraFront = glm::normalize(direction);

    g_View= glm::lookAt(cameraPos,cameraPos + cameraFront,glm::vec3(0,1,0));
    g_viewProjec=g_Projection*g_View;
}

