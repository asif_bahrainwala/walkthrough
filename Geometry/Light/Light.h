namespace Light
{
struct Square:Geom
{
    shared_ptr<LightPoint> m_lightPointAssociated;
    Square(float x,float y,float z,shared_ptr<LightPoint> l,shared_ptr<Program> p):Geom(p),m_lightPointAssociated(l)
    {   
        vector<MyVertex> m_vertexPoints;
        static float sz=.5;
        m_vertexPoints.push_back(MyVertex(-sz +x, -sz+y,  .5+z,  0,0,1, 0,0));
        m_vertexPoints.push_back(MyVertex(sz  +x,  sz+y,  .5+z,  0,0,1, 1,1));
        m_vertexPoints.push_back(MyVertex(-sz +x,  sz+y,  .5+z,  0,0,1, 0,1));

        m_vertexPoints.push_back(MyVertex(-sz +x, -sz+y, .5+z,   0,0,1, 0,0));
        m_vertexPoints.push_back(MyVertex(sz  +x,  sz+y, .5+z,   0,0,1, 1,1));
        m_vertexPoints.push_back(MyVertex(sz  +x, -sz+y, .5+z,   0,0,1, 1,0));


        glGenBuffers(1, &m_vertex);
        glBindBuffer(GL_ARRAY_BUFFER, m_vertex);
        glBufferData(GL_ARRAY_BUFFER, m_vertexPoints.size()*sizeof(MyVertex), &m_vertexPoints[0], GL_STATIC_DRAW);

        m_numberOfVerticestobeDrawn=m_vertexPoints.size();
    }

    void Draw(bool bTransparent,glm::mat4 mat)
    {
        m_lightPointAssociated->m_pos= mat*m_lightPointAssociated->m_orgpos;
        Geom::Draw(bTransparent,mat);
    }
};
}
