namespace Frame
{

#define GLSL(src) "#version 450\n" #src
const char* vertexShaderSrc = GLSL(
            in vec3 pos;
        in vec3 normal;
in vec2 tex;

uniform mat4 MVP;

out vec2 texOut;

void main() {
    gl_Position.xyz = pos.xyz;
    gl_Position.w=1.0f;
    texOut=tex;
}
);

const char* fragmentShaderSrc = GLSL(
            precision mediump float;

        uniform sampler2D colorMap0;//color map
uniform sampler2D colorMap1;//bloom texture
uniform sampler2D colorMap2;//position texure
uniform float step;

uniform sampler2D depthMap; //depth map

in vec2 texOut;
out vec4 FragColor;

void main() {
    float depth = texture(depthMap, texOut.xy,0.0).r;
    FragColor   = texture(colorMap0, texOut.xy,depth)/1.5;  //use depth to do some depth field
    FragColor   = FragColor+2.0*texture(colorMap1, texOut.xy,5.0); //adding bloom
}
);

struct Frame:Geom
{
    Frame(shared_ptr<Program> p):Geom(p)
    {
        vector<MyVertex> m_vertexPoints;
        m_vertexPoints.resize(6);

        m_vertexPoints[0]=MyVertex(-1,-1,  0,  0,0,1, 0,0);
        m_vertexPoints[1]=MyVertex(1 , 1,  0,  0,0,1, 1,1);
        m_vertexPoints[2]=MyVertex(-1  ,1, 0,  0,0,1, 0,1);

        m_vertexPoints[3]=MyVertex(-1  ,-1, 0, 0,0,1,  0,0);
        m_vertexPoints[4]=MyVertex(1  ,1, 0,   0,0,1,  1,1);
        m_vertexPoints[5]=MyVertex(1  ,-1, 0,  0,0,1,  1,0);

        glGenBuffers(1, &m_vertex);
        glBindBuffer(GL_ARRAY_BUFFER, m_vertex);
        glBufferData(GL_ARRAY_BUFFER, m_vertexPoints.size()*sizeof(MyVertex), &m_vertexPoints[0], GL_STATIC_DRAW);


        m_numberOfVerticestobeDrawn=m_vertexPoints.size();
    }

    virtual void Draw(bool,glm::mat4)
    {
        auto progID=m_program->GetID();
        glUseProgram(progID); //use the shaders

        static auto gli0=glGetUniformLocation(progID, "colorMap0");
        static auto gli1=glGetUniformLocation(progID, "colorMap1");
        static auto gli2=glGetUniformLocation(progID, "colorMap2");
        static auto gli3=glGetUniformLocation(progID, "depthMap");
        static auto gli4=glGetUniformLocation(progID, "step");

        glUniform1f(gli4, g_step);

        if(true == m_arrayvertexobjectCreated)
            glBindVertexArray(m_arrayvertexobject);
        else  //the below code needs to be called only once
        {
            m_arrayvertexobjectCreated=true;
            glBindVertexArray(m_arrayvertexobject);

            glBindBuffer(GL_ARRAY_BUFFER, m_vertex);
            GLint posAttrib =0;// glGetAttribLocation(progID, "pos");
            glEnableVertexAttribArray(posAttrib);
            glVertexAttribPointer(posAttrib, 3, GL_FLOAT, GL_FALSE, sizeof(MyVertex), 0);

            posAttrib = 2;//glGetAttribLocation(progID, "normal");
            glEnableVertexAttribArray(posAttrib);
            glVertexAttribPointer(posAttrib, 3, GL_FLOAT, GL_FALSE, sizeof(MyVertex),(void*)(3*sizeof(float)));

            posAttrib = 1;//glGetAttribLocation(progID, "tex");
            glEnableVertexAttribArray(posAttrib);
            glVertexAttribPointer(posAttrib, 2, GL_FLOAT, GL_FALSE, sizeof(MyVertex),(void*)(6*sizeof(float)));
        }

        glUniform1i(gli0, 0); glUniform1i(gli1, 1);glUniform1i(gli2, 2);glUniform1i(gli3, 3);
        for(size_t i=0;i<m_tex.size();++i)
            m_tex[i]->UseTexture(GL_TEXTURE0+i);

        glDrawArrays( GL_TRIANGLES, 0, m_numberOfVerticestobeDrawn );  //draw the frame
        glUseProgram(0); //use the shaders
    }
};
}
