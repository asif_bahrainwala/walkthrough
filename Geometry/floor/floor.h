namespace Floor
{
struct Floor:Geom
{
    Floor(shared_ptr<Program> p):Geom(p)
    {   
        vector<MyVertex> m_vertexPoints;
        static float sz=10;
        m_vertexPoints.push_back(MyVertex(-sz , -.5,  -sz, 0,1,0,   0,0));
        m_vertexPoints.push_back(MyVertex(sz  , -.5,  sz,  0,1,0,   1,1));
        m_vertexPoints.push_back(MyVertex(-sz , -.5,  sz,  0,1,0,   0,1));

        m_vertexPoints.push_back(MyVertex(-sz ,-.5, -sz,   0,1,0,   0,0));
        m_vertexPoints.push_back(MyVertex(sz  ,-.5,  sz,   0,1,0,   1,1));
        m_vertexPoints.push_back(MyVertex(sz  ,-.5, -sz,   0,1,0,   1,0));


        glGenBuffers(1, &m_vertex);
        glBindBuffer(GL_ARRAY_BUFFER, m_vertex);
        glBufferData(GL_ARRAY_BUFFER, m_vertexPoints.size()*sizeof(MyVertex), &m_vertexPoints[0], GL_STATIC_DRAW);

        m_numberOfVerticestobeDrawn=m_vertexPoints.size();
    }
};
}
