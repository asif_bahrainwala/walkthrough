namespace Wall
{
void MakeXWall(vector<MyVertex> &pvertex,float x=0,float z=0)
{
    static float sz=.5;
    pvertex.push_back(MyVertex(-sz +x,-sz,  z, 0,0,1,  0,0));
    pvertex.push_back(MyVertex(sz  +x, sz,  z, 0,0,1,  1,1));
    pvertex.push_back(MyVertex(-sz +x,sz,   z, 0,0,1,  0,1));

    pvertex.push_back(MyVertex(-sz +x,-sz, z,  0,0,1,   0,0));
    pvertex.push_back(MyVertex(sz  +x,sz,  z,  0,0,1,   1,1));
    pvertex.push_back(MyVertex(sz  +x,-sz, z,  0,0,1,   1,0));
}

void MakeZWall(vector<MyVertex> &pvertex,float x=-5,float z=0)
{
    static float sz=.5;
    pvertex.push_back(MyVertex(x,-sz,  -sz+z,  1,0,0, 0,0));
    pvertex.push_back(MyVertex(x, sz,  sz+z,   1,0,0, 1,1));
    pvertex.push_back(MyVertex(x,sz,   -sz+z,  1,0,0, 0,1));

    pvertex.push_back(MyVertex(x,-sz,-sz+z,    1,0,0, 0,0));
    pvertex.push_back(MyVertex(x,sz, sz+z,     1,0,0, 1,1));
    pvertex.push_back(MyVertex(x,-sz,sz+z,     1,0,0, 1,0));
}

struct walls:Geom
{
    walls(shared_ptr<Program> p):Geom(p)
    {   
        vector<MyVertex> m_vertexPoints;
        ifstream is("../walkthrough/Geometry/walls/roomWalls");
        char c=0;
        float x=-5,y=0;
        while(!is.eof())
        {
            is>>c;
            if('*'==c)
            {
                MakeXWall(m_vertexPoints,x,y);
            }
            else if('|'==c)
            {
                MakeZWall(m_vertexPoints,x,y);
            }
            else if('+'==c)
            {
                MakeXWall(m_vertexPoints,x,y);
                MakeZWall(m_vertexPoints,x,y);
            }
            else if('&'==c)
            {
                y++;
                x=-6;
            }
            x++;
        }

        glGenBuffers(1, &m_vertex);
        glBindBuffer(GL_ARRAY_BUFFER, m_vertex);
        glBufferData(GL_ARRAY_BUFFER, m_vertexPoints.size()*sizeof(MyVertex), &m_vertexPoints[0], GL_STATIC_DRAW);

        m_numberOfVerticestobeDrawn=m_vertexPoints.size();
    }
};
}
