namespace Cube
{
struct cube:Geom
{
    cube(shared_ptr<Program> p):Geom(p)
    {
        float f=.5;
        MyVertex pvertex[8]=
                {
                        {-f, f, -f,   0,0,0,  	0.0f, 1.0f  	},
                        {-f, -f, -f,  0,0,0,	0.0f, 0.0f,		},
                        {f, f, -f,	  0,0,0,	1.0f, 1.0f,		},
                        {f, -f, -f,	  0,0,0,    1.0f, 0.0f  	},

                        {-f, f, f,	  0,0,0,    0.0f, 1.0f		},
                        {f, f, f,	  0,0,0,    1.0f, 1.0f		},
                        {-f, -f, f,   0,0,0,    0.0f, 0.0f   	},
                        {f, -f, f,	  0,0,0,    1.0f, 0.0f		}
                    };

        vector<unsigned int> m_skyboxIndices={
                        // front
                        0, 1, 2,
                        2, 1, 3,
                        // right
                        2, 3, 5,
                        5, 3, 7,
                        // back
                        5, 7, 4,
                        4, 7, 6,
                        // left
                        4, 6, 0,
                        0, 6, 1,
                        // top
                        4, 0, 5,
                        5, 0, 2,
                        // bottom
                        1, 6, 3,
                        3, 6, 7
                    };

        glGenBuffers(1, &m_vertex);
        glBindBuffer(GL_ARRAY_BUFFER, m_vertex);
        glBufferData(GL_ARRAY_BUFFER, sizeof(pvertex), &pvertex[0], GL_STATIC_DRAW);

        m_numberOfVerticestobeDrawn=8;

        glGenBuffers(1, &m_Index);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_Index);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(m_skyboxIndices[0]) * m_skyboxIndices.size(), &m_skyboxIndices[0], GL_STATIC_DRAW);

        m_numberOfIndicestobeDrawn=m_skyboxIndices.size();
    }
};

#define GLSL(src) "#version 430\n" #src
const char* vertexShaderSrc = GLSL(
in vec3 pos;

uniform mat4 MVP;
uniform mat4 WORLD;

out vec3 texOut;

void main() {
    gl_Position.xyz = pos.xyz;
    gl_Position.w=1.0f;
    gl_Position=MVP*WORLD*gl_Position;
    texOut=pos.xyz;
}
);

const char* fragmentShaderSrc = GLSL(
uniform samplerCube colorMap0;
out vec4 FragColor;
out vec4 FragColor1;  //this is bloom text
out vec4 FragColor2;  //this is position text

in vec3 texOut;

void main() {
    FragColor = texture(colorMap0,texOut);
    FragColor1 = vec4(0,0,0,0);
    FragColor2 = vec4(0,0,0,0);
}
);
}
