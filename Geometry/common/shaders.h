namespace StandardShader
{
#define GLSL(src) "#version 300 es\n" #src
const char* vertexShaderSrc = GLSL(
in vec3 pos;
in vec3 normal;
in vec2 tex;

uniform mat4 MVP;
uniform mat4 WORLD;

out vec2 texOut;
out vec3 posout;
out vec3 normalout;

void main() {
    gl_Position.xyz = pos.xyz;
    gl_Position.w=1.0f;
    gl_Position=MVP*WORLD*gl_Position;

    vec4 posouttemp=vec4(pos.xyz,1);
    posouttemp=WORLD*posouttemp;
    posout=posouttemp.xyz;
    texOut=tex;

    normalout=(MVP*WORLD*vec4(normal,1.0)).xyz;
}
);

const char* fragmentShaderSrc = GLSL(
precision mediump float;

uniform sampler2D colorMap0;
//uniform sampler2D colorMap1;
//uniform sampler2D colorMap2;

uniform vec3 lightPos0;  //this is format of point lights used
uniform vec3 lightColor0;
uniform vec3 lightPos1;
uniform vec3 lightColor1;
uniform vec3 lightPos2;
uniform vec3 lightColor2;

uniform vec3 spotlightcol;
uniform vec3 spotlightpos;
uniform vec3 spotlightlookat;

uniform float step;

in vec2 texOut;
in vec3 posout;
in vec3 normalout;

out vec4 FragColor;
out vec4 FragColor1;  //this is bloom text
out vec4 FragColor2;  //this is position text

void main() {
    FragColor = (texture(colorMap0, texOut.xy,0.0));//+texture(colorMap1, texOut.xy,0.0)+texture(colorMap2, texOut.xy,0.0))/3.0;

    if(FragColor.r>.9 || FragColor.g>.9 || FragColor.b>.9)
        FragColor1=FragColor;
    else
        FragColor1=vec4(0.0,0.0,0.0,0.0);

    float dist0=length(lightPos0-posout);
    vec3 lightColort=lightColor0/(10.0*dist0*dist0*dist0);
    FragColor=FragColor+vec4(lightColort,FragColor.a);

    float dist1=length(lightPos1-posout);
    lightColort=lightColor1/(10.0*dist1*dist1*dist1);
    FragColor=FragColor+vec4(lightColort,FragColor.a);

    float dist2=length(lightPos2-posout);
    lightColort=lightColor2/(10.0*dist2*dist2*dist2);
    FragColor=FragColor+vec4(lightColort,FragColor.a);

    FragColor2=vec4((1.0-posout.xyz)/2.0,1.0);//this can be moved to VS

    //lets play with some spot light
    vec3 direction    = spotlightlookat-spotlightpos;

    float dist=length(spotlightpos - posout);
    if(dist<1.0) dist=1.0;
    vec3 lightDir = normalize(spotlightpos - posout);
    float theta = dot(lightDir, -normalize(direction));

    if(theta > 0.997)
        FragColor=FragColor+vec4(spotlightcol/(dist*dist),0);

}
);
}
