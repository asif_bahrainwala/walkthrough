//a helper function to create dancing light
template<int i1>
glm::mat4 rotateY(float f)
{
    glm::mat4 mat=glm::identity<glm::mat4>();
    static float angle =0;angle=angle+f;
    mat = glm::rotate(mat, angle, glm::vec3(0.0f, 1.0f, 0.0f));
    return mat;
}
