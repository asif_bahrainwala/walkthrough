#ifndef GLFWWINDOW_H
#define GLFWWINDOW_H
#include "openglSmartObjects.h"
#include <GLFW/glfw3.h>
using namespace std;

struct OpenGLWindow
{
    struct Appstartup{
        Appstartup()
        {
            if (!glfwInit()) exit(-1);
        }
        ~Appstartup()
        {
            glfwPollEvents();
            glfwTerminate();
        }
    };

    GLFWwindow *m_window;
    OpenGLWindow()
    {
        static Appstartup start;

        //glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
        //glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);
        m_window = glfwCreateWindow(WIDTH, HEIGHT, "Hello World", NULL, NULL);
        glfwSetKeyCallback(m_window, key_callback);
        glfwSetCursorPosCallback(m_window, mouse_callback);
        glfwMakeContextCurrent(m_window);

        ++g_count;
    }

    ~OpenGLWindow()
    {
        glfwDestroyWindow(m_window);
        --g_count;
    }

    void OpenGLWindowSwapBuffers(){glfwSwapBuffers(m_window);}

    static void mouse_callback(GLFWwindow* window, double xpos, double ypos);
    static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods);
};

#endif // GLFWWINDOW_H
