#include "openglSmartObjects.h"

GLuint Program::shaderCompileFromFile(GLenum type,const char *source)
{
    GLuint shader;
    GLint  result;

    /* create shader object, set the source, and compile */
    shader = glCreateShader(type);
    GLint length = strlen(source);
    glShaderSource(shader, 1, (const char **)&source, &length);
    glCompileShader(shader);

    /* make sure the compilation was successful */
    glGetShaderiv(shader, GL_COMPILE_STATUS, &result);
    if(result == GL_FALSE) {
        char *log;
        /* get the shader info log */
        glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &length);
        log = (char*)malloc(length);
        glGetShaderInfoLog(shader, length, &result, log);

        /* print an error message and the info log */
        printf("shaderCompileFromFile(): Unable to compile %s: %s\n",source,log);
        free(log);

        glDeleteShader(shader);
        return 0;
    }

    return shader;
}

void Program::shaderAttachFromFile(GLenum type, const char *shaderSource)
{
    if(nullptr==shaderSource) return;

    /* compile the shader */
    GLuint shader = shaderCompileFromFile(type, shaderSource);
    if(shader != 0) {
        glAttachShader(m_programID, shader);     /* attach the shader to the program */

        /* delete the shader - it won't actually be
         * destroyed until the program that it's attached
         * to has been destroyed */
        glDeleteShader(shader);
        shader=0;
    }
}

void Program::GetLinkStatus()
{
    GLint result=0;glGetProgramiv(m_programID, GL_LINK_STATUS, &result);
    if(result == GL_FALSE) {
        GLint length;
        char *log;
        /* get the program info log */
        glGetProgramiv(m_programID, GL_INFO_LOG_LENGTH, &length);
        log = (char*)malloc(length);
        glGetProgramInfoLog(m_programID, length, &result, log);
        /* print an error message and the info log */
        fprintf(stderr, "Program linking failed: %s\n", log);
        free(log);
    }
}

void Geom::Draw(bool bTransparent,glm::mat4 mat)
{
    auto progID=m_program->GetID();
    glUseProgram(progID); //use the shaders

    if(m_MovewithEye)
    {
        glm::mat4 tra= glm::identity<glm::mat4>();
        tra = glm::translate<float>(tra, cameraPos);
        g_viewProjec=g_viewProjec*tra;
    }

    {
    static auto gli = glGetUniformLocation(progID, "MVP");  //no need to query this location again (marked as static)
    glUniformMatrix4fv(gli, 1, GL_FALSE, &g_viewProjec[0][0]);

    static auto gli1 = glGetUniformLocation(progID, "WORLD");
    glUniformMatrix4fv(gli1, 1, GL_FALSE, &mat[0][0]);
    }

    if(m_MovewithEye)
    {
        glm::mat4 tra= glm::identity<glm::mat4>();
        tra = glm::translate<float>(tra, cameraPos);
        tra=  glm::inverse(tra);
        g_viewProjec=g_viewProjec*tra;  //undo the above translation
    }

    if(m_spotLight.get())
    {
        glm::vec3 spotlightcol=m_spotLight->m_spotlightcol;
        static auto gli0 = glGetUniformLocation(progID, "spotlightcol");
        glUniform3f(gli0,spotlightcol.x,spotlightcol.y,spotlightcol.z);

        glm::vec3 spotlightpos=m_spotLight->m_spotlightpos;
        static auto gli1= glGetUniformLocation(progID, "spotlightpos");
        glUniform3f(gli1,spotlightpos.x,spotlightpos.y,spotlightpos.z);

        glm::vec3 spotlightlookat=m_spotLight->m_spotlightlookat;
        static auto gli2 = glGetUniformLocation(progID, "spotlightlookat");
        glUniform3f(gli2,spotlightlookat.x,spotlightlookat.y,spotlightlookat.z);
    }


    for(size_t i=0;i<m_lightpoints.size();++i)
    {
        stringstream sslightPos,lightColor;
        sslightPos<<"lightPos"<<i;
        lightColor<<"lightColor"<<i;
        auto gli = glGetUniformLocation(progID, sslightPos.str().c_str());
        glUniform3f(gli,m_lightpoints[i]->m_pos.x,m_lightpoints[i]->m_pos.y,m_lightpoints[i]->m_pos.z);

        gli = glGetUniformLocation(progID, lightColor.str().c_str());
        glUniform3f(gli,m_lightpoints[i]->m_lightcolor.x,m_lightpoints[i]->m_lightcolor.y,m_lightpoints[i]->m_lightcolor.z);
    }

    {
        static auto gli=glGetUniformLocation(progID, "step");
        glUniform1f(gli, g_step);
    }

    if(true == m_arrayvertexobjectCreated)
        glBindVertexArray(m_arrayvertexobject);
    else  //below code needs to be called only once
    {
        m_arrayvertexobjectCreated=true;

        glBindVertexArray(m_arrayvertexobject);

        glBindBuffer(GL_ARRAY_BUFFER, m_vertex);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_Index);

        GLint posAttrib = 0;//glGetAttribLocation(progID, "pos");
        glEnableVertexAttribArray(posAttrib);
        glVertexAttribPointer(posAttrib, 3, GL_FLOAT, GL_FALSE, sizeof(MyVertex), 0);

        posAttrib = 2;//glGetAttribLocation(progID, "normal");
        glEnableVertexAttribArray(posAttrib);
        glVertexAttribPointer(posAttrib, 3, GL_FLOAT, GL_FALSE, sizeof(MyVertex),(void*)(3*sizeof(float)));

        posAttrib = 1;//glGetAttribLocation(progID, "tex");
        glEnableVertexAttribArray(posAttrib);
        glVertexAttribPointer(posAttrib, 2, GL_FLOAT, GL_FALSE, sizeof(MyVertex),(void*)(6*sizeof(float)));
    }

    for(size_t i=0;i<m_tex.size();++i)
        m_tex[i]->UseTexture(GL_TEXTURE0+i);

    static auto gli=glGetUniformLocation(progID, "colorMap0");
    glUniform1i(gli, 0);

    if(bTransparent)
    {
        glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
        glEnable(GL_BLEND);
    }
    if(0==m_numberOfIndicestobeDrawn)
        glDrawArrays( GL_TRIANGLES, 0, m_numberOfVerticestobeDrawn );  //draw the frame
    else
        glDrawElements(GL_TRIANGLES, m_numberOfIndicestobeDrawn, GL_UNSIGNED_INT, 0);

    if(bTransparent)
        glDisable(GL_BLEND);

    glUseProgram(0); //use the shaders
}


