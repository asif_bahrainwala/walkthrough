#include <chrono>
#include "GLFWWindow.h"
#include "Geometry/common/shaders.h"

#include "Geometry/walls/wall.h"
#include "Geometry/Frame/Frame.h"
#include "Geometry/floor/floor.h"
#include "Geometry/Light/Light.h"
#include "Geometry/cube/cube.h"
#include "Geometry/common/randome_helper.h"

#include "imgui.h"
#include "imgui_impl_glfw.h"
#include "imgui_impl_opengl3.h"

float g_step=0;  //this keeps track of steps (basically gives time input)

void DrawCandles(vector<shared_ptr<Geom>> &candle)
{
    for(size_t i=0;i<candle.size();++i)
    {
        auto mat=rotateY<0>(.05);
        candle[i]->Draw(true,mat); //draw transparent objects last
    }
}

void CameraDance()
{
    if(cameraDance)
    {
        static bool runonce=[=](){srand(chrono::system_clock::now().time_since_epoch().count());return true;}();
        static int i=0;
        static int x=5,y=5,x1=0,y1=0;
        if(i%500==0)
        {
            x=rand()%10;
            y=rand()%10;

            x1=5-rand()%10,y1=5-rand()%10;
        }
        ++i;

        cameraPos=glm::vec3(cos(g_step*x)*10, 3.0f,  sin(g_step*y)*10);
        g_viewProjec=g_Projection*glm::lookAt(cameraPos,glm::vec3(x1,0,y1),glm::vec3(0,1,0));
    }
}

int g_count=0;

int main()
{
    {
        OpenGLWindow w;

        frameBuffer f(3);  //use 3 color attachments
        //1) color attachment;
        //2) Bloom texture;
        //3) Position texture (we can use this for some effects like Point lights in FS)
        //+m_depthtex

        shared_ptr<Program> pCommon(new Program());
        pCommon->setShaderSource(StandardShader::vertexShaderSrc,nullptr,nullptr,nullptr,StandardShader::fragmentShaderSrc);

        //Creating the walls
        shared_ptr<Texture> walltex=make_shared<Texture>("../walkthrough/Geometry/walls/wall.jpeg");
        shared_ptr<Geom> walls(new Wall::walls(pCommon));
        walls->m_tex.push_back(walltex);


        //now creating the frame, this is a Rectangle geometry that uses textures output from FB
        shared_ptr<Program> pFrame(new Program());;
        pFrame->setShaderSource(Frame::vertexShaderSrc,nullptr,nullptr,nullptr,Frame::fragmentShaderSrc);
        shared_ptr<Geom> frame(new Frame::Frame(pFrame));
        frame->m_tex.push_back(f.m_tex[0]); //use the texture from framebuffer
        frame->m_tex.push_back(f.m_tex[1]); //use the bloom texture from framebuffer
        frame->m_tex.push_back(f.m_tex[2]); //use the position texture from framebuffer

        frame->m_tex.push_back(f.m_depthtex); //use the texture from framebuffer


        //now creating the floor
        shared_ptr<Texture> floortex=make_shared<Texture>("../walkthrough/Geometry/floor/woodfloor.jpeg");
        shared_ptr<Geom> Floor(new Floor::Floor(pCommon));
        Floor->m_tex.push_back(floortex);

        //2 point light
        vector<shared_ptr<LightPoint>> pointLight;

        //now creating a candles
        shared_ptr<Texture> lighTex=make_shared<Texture>("../walkthrough/Geometry/Light/diwali-png-4049.png");

        vector<shared_ptr<Geom>> candle(3);

        for(size_t i=0;i<candle.size();++i)
        {
            glm::vec4 pos=glm::vec4(2+i,0,2+i,1);
            auto pl=make_shared<LightPoint>(pos,glm::vec3(i/10.0,i/2.0,1-i/3.0));
            pointLight.push_back(pl);
            candle[i]=make_shared<Light::Square>(pos.x,pos.y,pos.z,pl,pCommon);
            candle[i]->m_tex.push_back(lighTex);
        }

        walls->m_lightpoints=pointLight;
        Floor->m_lightpoints=pointLight;

        //Creating one sport light (we only support 1)  (candels will not have spot light)
        shared_ptr<SpotLight> spotlight=make_shared<SpotLight>();
        spotlight->m_spotlightcol=glm::vec3(30,30,30);
        Floor->m_spotLight=walls->m_spotLight=spotlight;

        shared_ptr<Program> pCubeShader(new Program());
        pCubeShader->setShaderSource(Cube::vertexShaderSrc,nullptr,nullptr,nullptr,Cube::fragmentShaderSrc);

        shared_ptr<Texture> cubetex=make_shared<Texture>(vector<string>{"../walkthrough/Geometry/cube/1.png","../walkthrough/Geometry/cube/1.png","../walkthrough/Geometry/cube/1.png","../walkthrough/Geometry/cube/1.png","../walkthrough/Geometry/cube/1.png","../walkthrough/Geometry/cube/1.png"}); //https://www.khronos.org/opengl/wiki/Cubemap_Texture
        Cube::cube skybox(pCubeShader);
        skybox.m_tex.push_back(cubetex);
        skybox.m_MovewithEye=true;  //this will cause the object to be translated according to camera position (used in skyboxes)

        //this code is picked up from https://github.com/ocornut/imgui/tree/master/examples/example_glfw_opengl3
        //imgui related
        IMGUI_CHECKVERSION();
        ImGui::CreateContext();
        // Setup Dear ImGui style
        ImGui::StyleColorsDark();
        //ImGui::StyleColorsLight();
        ImGui_ImplGlfw_InitForOpenGL(w.m_window, true);
        const char* glsl_version = "#version 100";
        ImGui_ImplOpenGL3_Init(glsl_version);
        bool show_demo_window = true;
        ImVec4 clear_color = ImVec4(0.45f, 0.55f, 0.60f, 1.00f);
        ImGuiIO& io = ImGui::GetIO();
        io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;     // Enable Keyboard Controls
        io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;      // Enable Gamepad Controls

        while (!glfwWindowShouldClose(w.m_window))
        {
            ImGui_ImplOpenGL3_NewFrame();
            ImGui_ImplGlfw_NewFrame();
            ImGui::NewFrame();
            ImGui::ShowDemoWindow(&show_demo_window);
            ImGui::Render();

            g_step=g_step+.001;

            CameraDance();

            //spinning the spot light around
            spotlight->m_spotlightpos=glm::vec3(cos(g_step*5.0)*10.0+10.0,2.0,sin(g_step*5.0)*10.0+10.0);

            f.UseFrameBuffer();

            glClearColor( 0, 0, 0.0f, 0.0f );
            glClearDepth(1.0f);   //is always to set to 1.0 when clearing
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
            glEnable(GL_DEPTH_TEST);

            //do all OpenGL work here
            {
                glDisable(GL_DEPTH_TEST);  //disable the Z test
                skybox.Draw();             //skybox must be drawn first
                glEnable(GL_DEPTH_TEST);   //enable the Z test

                walls->Draw();
                Floor->Draw();
                DrawCandles(candle);
            }

            f.StopUseFrameBuffer();
            //f.m_tex[0]->SaveToFile();

            glClearColor( 1, .3, 0.0f, 0.0f );
            glClearDepth(1.0f);   //is always to set to 1.0 when clearing
            glClear( GL_COLOR_BUFFER_BIT |GL_DEPTH_BUFFER_BIT);
            glEnable(GL_DEPTH_TEST);

            frame->Draw();
            ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

            w.OpenGLWindowSwapBuffers();
            glfwPollEvents();
        }

    }

    // Cleanup
    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplGlfw_Shutdown();
    ImGui::DestroyContext();

    cout<<"object count:"<<g_count<<endl;
    return 0;
}

